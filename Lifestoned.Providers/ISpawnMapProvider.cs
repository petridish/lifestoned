﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Gdle.Spawns;

namespace Lifestoned.Providers
{
    public interface ISpawnMapProvider
    {
        SpawnMapEntry GetLandblock(uint id);
        void SaveLandblock(SpawnMapEntry spawnMap);
        void DeleteLandblock(uint id);

        List<SpawnMapEntry> SearchLandblocks(string query);
        IEnumerable<SpawnMapEntry> GetLandblocks();
    }

	public interface ISpawnMapSandboxProvider : ISpawnMapProvider
    {
        SpawnMapChange GetLandblockChange(uint id);
        SpawnMapChange GetLandblockChange(uint id, string userId);
    }
}
