﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;

using Lifestoned.DataModel.Gdle.Spawns;

namespace Lifestoned.Providers.Database
{
    public class DatabaseSpawnMapProvider : SQLiteDatabaseProvider, ISpawnMapProvider
    {
        public DatabaseSpawnMapProvider() : base("SpawnMapDbConnection")
        {
        }

        protected override void OnInitialize(DbConnection connection)
        {
            connection.Execute(@"
				CREATE TABLE IF NOT EXISTS SpawnMaps (
				landblockId INT PRIMARY KEY,
				lastModified DATETIME,
				userModified VARCHAR(100),
				jsonData TEXT);");
        }

        #region ISpawnMapProvider

        public void DeleteLandblock(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute("DELETE FROM SpawnMaps WHERE landblockId=@id", new { id });
            }
            catch (DbException)
            {
            }
        }

        public SpawnMapEntry GetLandblock(uint id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SpawnMapEntry> GetLandblocks()
        {
            throw new NotImplementedException();
        }

        public void SaveLandblock(SpawnMapEntry spawnMap)
        {
            throw new NotImplementedException();
        }

        public List<SpawnMapEntry> SearchLandblocks(string query)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
