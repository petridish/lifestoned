﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.Providers.Database
{
    public class SQLiteDatabaseProvider : DatabaseProviderBase
    {
		protected SQLiteDatabaseProvider(string connectionName) : base(connectionName)
        {
        }

        protected override void OnSetupConnection(DbConnection connection)
        {
            SQLiteConnection db = connection as SQLiteConnection;
            if (db != null)
            {
                //db.EnableExtensions(true);
                //db.LoadExtension("SQLite.Interop.dll", "sqlite3_json_init");
            }
        }
    }

	public class SQLiteContentDatabase<T> : SQLiteDatabaseProvider, IGenericContentProvider<T> where T : class, IMetadata
    {
		protected string TableName { get; set; }

        private Func<T, uint> getId;

		public SQLiteContentDatabase(string connectionName, string tableName, Func<T, uint> idfn) : base(connectionName)
        {
            TableName = tableName;
            getId = idfn;
        }

        protected override void OnInitialize(DbConnection connection)
        {
            connection.Execute($@"
				CREATE TABLE IF NOT EXISTS {TableName} (
				id INT PRIMARY KEY,
				lastModified DATETIME,
				userModified VARCHAR(100) COLLATE NOCASE,
				jsonData TEXT COLLATE NOCASE);");
        }

        #region IGenericContentProvider

        public bool Delete(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                    db.Execute($"DELETE FROM {TableName} WHERE id=@id", new { id });
                return true;
            }
            catch (DbException)
            {
                return false;
            }
        }

        public IQueryable<T> Get()
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    var query = db.Query<string>($"SELECT jsonData FROM {TableName}");
                    return query.Select(content => JsonConvert.DeserializeObject<T>(content)).AsQueryable();
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public T Get(uint id)
        {
            try
            {
                using (DbConnection db = GetConnection())
                {
                    string content = db.ExecuteScalar<string>($"SELECT jsonData FROM {TableName} WHERE id=@id", new { id });
                    return JsonConvert.DeserializeObject<T>(content);
                }
            }
            catch (DbException)
            {
                return null;
            }
        }

        public bool Save(T item)
        {
            return Update(item);
        }

        public bool Update(T item)
        {
            try
            {
                string content = JsonConvert.SerializeObject(item);

                using (DbConnection db = GetConnection())
                {
                    db.Execute($@"
						INSERT INTO {TableName} (id, lastModified, userModified, jsonData)
						VALUES (@id, @LastModified, @ModifiedBy, @content)
						ON CONFLICT (id) DO UPDATE SET
						lastModified = @LastModified, userModified = @ModifiedBy, jsonData = @content
						WHERE id = @id;
						", new
                    {
                        id = getId(item),
                        item.LastModified,
                        item.ModifiedBy,
                        content
                    });
                    return true;
                }
            }
            catch (DbException ex)
            {
                return false;
            }
        }

        #endregion

    }
}
