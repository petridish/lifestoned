﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lifestoned.DataModel.Content
{
    //    CREATE TABLE IF NOT EXISTS PatchContent
    public class PatchContent
    {
		//    patchId INT NOT NULL
		public int PatchId { get; set; }
		
		//    contentType INT NOT NULL
		public Shared.ContentType ContentType { get; set; }

		//    contentId INT NOT NULL
		public int ContentId { get; set; }

        //    lastModified DATETIME,
        public DateTime LastModified { get; set; }

        //    userModified VARCHAR(100)
        public string UserModified { get; set; }

        //    name VARCHAR(250)
        public string Name { get; set; }

        //    userAssigned BINARY(16)
        public Guid UserAssigned { get; set; }

        //    complete INT DEFAULT 0 NOT NULL
        public bool Complete { get; set; }

        //    completeDate DATETIME
        public DateTime CompleteDate { get; set; }
    }
}
