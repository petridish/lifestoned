﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.DataModel.Gdle.Spawns
{
    public class SpawnMap
    {
        [JsonProperty("_comment")]
        public string Comment { get; set; }

        [JsonProperty("landblocks")]
        public List<SpawnMapEntry> Entries { get; set; } = new List<SpawnMapEntry>();
    }

    public class SpawnMapEntry : IMetadata
    {
        [JsonProperty("key")]
        public uint Key { get; set; }

        [JsonProperty("value")]
        public SpawnMapData Value { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        #region Metadata

        [JsonProperty("lastModified")]
        [Display(Name = "Last Modified Date")]
        public DateTime? LastModified { get; set; }

        [JsonProperty("modifiedBy")]
        [Display(Name = "Last Modified By")]
        public string ModifiedBy { get; set; }

        [JsonProperty("changelog")]
        public List<ChangelogEntry> Changelog { get; set; } = new List<ChangelogEntry>();

        [JsonProperty("userChangeSummary")]
        public string UserChangeSummary { get; set; }

        [JsonProperty("isDone")]
        [Display(Name = "Is Done")]
        public bool IsDone { get; set; }

        [JsonProperty("comments")]
        public string Comments { get; set; }

        #endregion

    }

    public class SpawnMapData
    {
        [JsonProperty("links")]
        public List<SpawnLink> Links { get; set; } = new List<SpawnLink>();

        [JsonProperty("weenies")]
        public List<SpawnWeenie> Weenies { get; set; } = new List<SpawnWeenie>();
    }

    public class SpawnMapChange : ChangeEntry<SpawnMapEntry>
    {
        public const string TypeName = "spawnmap";

        [JsonIgnore]
        public override uint EntryId
        {
            get => Entry.Key;
            set => Entry.Key = value;
        }

        [JsonIgnore]
        public override string EntryType => SpawnMapChange.TypeName;

        [JsonProperty("spawnMap")]
        public override SpawnMapEntry Entry { get; set; }
    }

}
